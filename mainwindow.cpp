#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtGui>
#include <QFileDialog>
#include <QMessageBox>
#include <QPushButton>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    isSaved = false; //初始化文件为未保存过状态

    curFile = tr("未命名.txt"); //初始化文件名为“未命名.txt”

    setWindowTitle(curFile); //初始化主窗口的标题

    init_statusBar();

    connect(ui->textEdit,SIGNAL(cursorPositionChanged()),this,SLOT(do_cursorChanged()));

}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::do_file_New() //实现新建文件的功能
{
    do_file_SaveOrNot();
    isSaved = false;
    curFile = tr("未命名.txt");
    setWindowTitle(curFile);
    ui->textEdit->clear(); //清空文本编辑器
    ui->textEdit->setVisible(true); //文本编辑器可见
}

void MainWindow::do_file_SaveOrNot() //弹出是否保存文件对话框
{
    if(ui->textEdit->document()->isModified()) //如果文件被更改过,弹出保存对话框
    {
        QMessageBox box;
        box.setWindowTitle(tr("警告"));
        box.setIcon(QMessageBox::Warning);box.setText(curFile + tr("尚未保存,是否保存?"));
        box.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        if(box.exec() == QMessageBox::Yes) //如果选择保存文件,则执行保存操作
        do_file_Save();
    }
}

void MainWindow::do_file_Save() //保存文件
{
    if(isSaved){ //如果文件已经被保存过,直接保存文件
    saveFile(curFile);
}
    else
    {
        do_file_SaveAs(); //如果文件是第一次保存,那么调用另存为
    }
}

void MainWindow::do_file_SaveAs() //文件另存为
{
    QString fileName = QFileDialog::getSaveFileName(this,tr("另存为"),curFile);;
    //获得文件名
    if(!fileName.isEmpty()) //如果文件名不为空,则保存文件内容
    {
        saveFile(fileName);
    }
}

bool MainWindow::saveFile(const QString& fileName)
//保存文件内容,因为可能保存失败,所以具有返回值,来表明是否保存成功
{
    QFile file(fileName);
    if(!file.open(QFile::WriteOnly | QFile::Text))
    //以只写方式打开文件,如果打开失败则弹出提示框并返回
    {
        QMessageBox::warning(this,tr("保存文件"),
        tr("无法保存文件 %1:\n %2").arg(fileName)
        .arg(file.errorString()));
        return false;
    }
    //%1,%2 表示后面的两个 arg 参数的值

    QTextStream out(&file);
    //新建流对象,指向选定的文件

    out << ui->textEdit->toPlainText();
    //将文本编辑器里的内容以纯文本的形式输出到流

    isSaved = true;

    curFile = QFileInfo(fileName).canonicalFilePath(); //获得文件的标准路径

    setWindowTitle(curFile); //将窗口名称改为现在窗口的路径

    second_statusLabel->setText(tr("success to save file"));

    return true;
}


void MainWindow::show_findText()
{
    QString findText = find_textLineEdit->text();

    if(!ui->textEdit->find(findText,QTextDocument::FindBackward))
    {
        QMessageBox::warning(this,tr("find"),tr("can not find %1").arg(findText));
    }
}


void MainWindow::do_cursorChanged()
{
    int rowNum = ui->textEdit->document()->blockCount();

    const QTextCursor cursor = ui->textEdit->textCursor();

    int colNum = cursor.columnNumber();

    first_statusLabel->setText(tr("%1 row %2 col").arg(rowNum).arg(colNum));
}

void MainWindow::init_statusBar()
{
    QStatusBar *bar = ui->statusBar;

    first_statusLabel = new QLabel;

    first_statusLabel->setMinimumSize(150,20);

    first_statusLabel->setFrameShape(QFrame::WinPanel);

    first_statusLabel->setFrameShadow(QFrame::Sunken);

    second_statusLabel = new QLabel;

    second_statusLabel->setMinimumSize(150,20);

    second_statusLabel->setFrameShape(QFrame::WinPanel);

    second_statusLabel->setFrameShadow(QFrame::Sunken);

    bar->addWidget(first_statusLabel);

    bar->addWidget(second_statusLabel);

    first_statusLabel->setText(tr("Welcome to use the text editor"));

    second_statusLabel->setText(tr("made by Charlie"));
}




void MainWindow::on_actionNew_N_triggered()
{
    do_file_New();
}


void MainWindow::on_actionSave_S_triggered()
{
    do_file_Save();
}

void MainWindow::on_actionSave_As_A_triggered()
{
    do_file_SaveAs();
}

void MainWindow::do_file_Open()        //打开文件
{
    do_file_SaveOrNot();                     //判断是否需要保存现有文件
    QString fileName = QFileDialog::getOpenFileName(this);
    //获得要打开文件的名字
    if(!fileName.isEmpty())                  //如果文件名不为空
    {
        do_file_Load(fileName);
    }

    ui->textEdit->setVisible(true);           //文本编辑器可见
}

bool MainWindow::do_file_Load(const QString& fileName)           //读取文件
{
    QFile file(fileName);

    if(!file.open(QFile::ReadOnly) | QFile::Text)
    {
        QMessageBox::warning(this, tr("读取文件"), tr("无法读取文件 %1:\n%2. ").arg(fileName).arg(file.errorString()));

        return false;                     //如果打开文件失败，弹出对话框并返回
    }

    QTextStream in(&file);

    ui->textEdit->setText(in.readAll());     //将文件中的所有内容都写到文本编辑器中

    curFile = QFileInfo(fileName).canonicalFilePath();

    setWindowTitle(curFile);

    second_statusLabel->setText(tr("success to open file"));

    return true;
}

void MainWindow::on_actionOpen_O_triggered()
{
    do_file_Open();
}

void MainWindow::on_actionClose_C_triggered()
{
    do_file_SaveOrNot();

    ui->textEdit->setVisible(false);

    first_statusLabel->setText(tr("text editor was closed"));

    second_statusLabel->setText(tr("made by Charlie"));
}

void MainWindow::on_actionExit_X_triggered()
{
    on_actionClose_C_triggered();

    qApp->quit();
}

void MainWindow::on_actionCancel_Z_triggered()
{
    ui->textEdit->undo();
}

void MainWindow::on_actionCut_X_triggered()
{
    ui->textEdit->cut();
}

void MainWindow::on_actionCopy_C_triggered()
{
    ui->textEdit->copy();
}

void MainWindow::on_actionPaste_V_triggered()
{
    ui->textEdit->paste();
}

void MainWindow::on_actionFind_F_triggered()
{
    QDialog *findDlg = new QDialog(this);
    findDlg->setWindowTitle(tr("find"));

    find_textLineEdit = new QLineEdit(findDlg);

    QPushButton *find_Btn = new QPushButton(tr("find the next one"),findDlg);

    QVBoxLayout* layout = new QVBoxLayout(findDlg);

    layout->addWidget(find_textLineEdit);

    layout->addWidget(find_Btn);

    findDlg->show();

    connect(find_Btn,SIGNAL(clicked()),this,SLOT(show_findText()));

    second_statusLabel->setText(tr("process on finding..."));
}
